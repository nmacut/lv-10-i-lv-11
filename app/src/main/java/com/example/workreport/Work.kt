package com.example.workreport

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Work (@PrimaryKey val id: UUID = UUID.randomUUID(),
                 var title: String = "",
                 var date: Date = Date(),
                 var isDone: Boolean = false)
