package com.example.workreport

import androidx.lifecycle.ViewModel

class WorkListViewModel : ViewModel() {

    private val workRepository = WorkRepository.get()

    init {
        /*
        for (i in 0 until 5) {
            val work = Work()
            work.title = "Work placeholder!"
            work.isDone =  i%2 == 0
            workRepository.addJob(work)
        }*/
        val work = Work()
        work.title = "PROMJENA"
        work.isDone = false
        workRepository.updateJob(work)
    }


    val jobsLiveData = workRepository.getJobs()
}