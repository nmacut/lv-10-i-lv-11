package com.example.workreport

import android.app.Application

class WorkIntentApplication : Application() {
    override fun onCreate(){
        super.onCreate()
        WorkRepository.initialize(this)
    }
}